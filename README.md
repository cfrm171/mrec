# mrec

Financial business is exposed to many types of risks due to the nature of business. To guard against the risk, financial institutions must hold capital in proportion to the potential risk. Market risk economic capital is intended to capture the value